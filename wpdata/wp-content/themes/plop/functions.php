<?php
add_action( 'wp_enqueue_scripts', 'plop_wp_enqueue_scripts' );

function plop_wp_enqueue_scripts() {

	$parenthandle = 'twentytwenty-style';
	$theme        = wp_get_theme();

    // Load parent CSS
	wp_enqueue_style(
        $parenthandle,
		get_template_directory_uri() . '/style.css', // https://plop.org/wp-content/themes/twentytwenty/style.css
		array(),
		$theme->parent()->get( 'Version' )
	);

    // Load child CSS (this theme)
	wp_enqueue_style(
        'plop-style',
		get_stylesheet_uri(), // https://plop.org/wp-content/themes/plop/style.css
		array( $parenthandle ),
		$theme->get( 'Version' )
	);
    
}